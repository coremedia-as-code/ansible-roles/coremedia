#!/usr/bin/env bash

RAW=$(ps --format pid,user:30,%mem,command ax \
  | grep -v PID \
  | sort --ignore-leading-blanks --numeric-sort --reverse --key=3 \
  | head -n15 \
  | awk '/[0-9]*/{print $1 ":" $2 ":" $4}')

printf "%-10s%-30s%-15s%s\n" "PID" "OWNER" "MEMORY" "COMMAND"
command
for i in ${RAW}
do
  PID=$(echo "${i}" | cut -d: -f1)
  OWNER=$(echo "${i}" | cut -d: -f2)
  COMMAND=$(echo "${i}" | cut -d: -f3)
  MEMORY=$(sudo pmap ${PID} | tail -n1 | awk '/[0-9]K/{print $2}')

  printf "%-10s%-30s%-15s%s\n" "${PID}" "${OWNER}" "${MEMORY}" "${COMMAND}"
done
